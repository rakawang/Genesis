from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm

from .models import customUser
from .validator import validate_email, validate_username
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate
from .models import Profile


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""

    username = forms.CharField(validators = [validate_username], required=True, label= 'Username', widget = forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(validators = [validate_email], required=True, label= "Email", widget = forms.EmailInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(
        required=True,  
        help_text = mark_safe(_(
            '<ul class="light " style="list-style-type: square;"><li style="margin-left: -10px">    <small id="hint_id_password1" class=" d-block form-text text-muted"> Password must contain at least 8 characters.    </small> </li><li style="margin-left: -10px"> <small id="hint_id_password1" class=" d-block form-text text-muted"> Password can’t be a commonly used password.</small> </li><li style="margin-left: -10px">    <small id="hint_id_password1" class=" d-block form-text text-muted"> Password can’t be too similar to your other personal information.</small> </li><li style="margin-left: -10px"><small id="hint_id_password1" class=" d-block form-text text-muted"> Password can’t be entirely numeric.</small></li></ul>'
        ))
        , label= "Password",  widget = forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label=_("Confirm Password"), widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = customUser
        fields = '__all__'
        exclude = ['last_login', 'password', 'is_admin']

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError("Passwords don't match")
        return password2

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)

        # for not displaying help text 
        for fieldname in ['username', 'password2']:
            self.fields[fieldname].help_text = None

        # adding class to password2 field
        self.fields['password2'].widget.attrs['class'] = 'form-control'

        # for placeholder
        self.fields['username'].widget.attrs['placeholder'] = 'Example: Genesis'
        self.fields['email'].widget.attrs['placeholder'] = 'Example@gmail.com'

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.username = (self.cleaned_data["username"])
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class normalUserCreation(UserCreationForm):
    class Meta:
        model = customUser
        fields = ['username', 'email', 'password1', 'password2']

class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = customUser
        fields = ('email', 'username', 'password', 'is_active', 'is_admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserLoginForm(AuthenticationForm):

    username = forms.CharField(max_length=254)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': _("Please enter a correct %(username)s and password. "
                           "Note that both fields may be case-sensitive."),
        'inactive': _("This account is inactive."),
    }

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username,
                                           password=password)
            if user is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login', 
                    params={'username': self.username_field.verbose_name},
                )
                
            if not user.is_active:
                raise forms.ValidationError(
                    self.error_messages['inactive'],
                    code='inactive',
                )

        return self.cleaned_data        
    
    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

class ProfileUpdateForm(forms.ModelForm):
    
    class Meta:
        model = Profile
        fields = '__all__'
        exclude = ['user',]

class passwordChangeForm(PasswordChangeForm):

    old_password = forms.CharField(label=_("Old Password"), widget=forms.PasswordInput)
    new_password1 = forms.CharField(label=_("New Password"),help_text = mark_safe(_(
            '<ul class="light " style="list-style-type: square;"><li style="margin-left: -10px">    <small id="hint_id_password1" class=" d-block form-text text-muted"> Password must contain at least 8 characters.    </small> </li><li style="margin-left: -10px"> <small id="hint_id_password1" class=" d-block form-text text-muted"> Password can’t be a commonly used password.</small> </li><li style="margin-left: -10px">    <small id="hint_id_password1" class=" d-block form-text text-muted"> Password can’t be too similar to your other personal information.</small> </li><li style="margin-left: -10px"><small id="hint_id_password1" class=" d-block form-text text-muted"> Password can’t be entirely numeric.</small></li></ul>'
        )),
        widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=_("Confirm Password"), widget=forms.PasswordInput)

    class Meta:
        model = customUser
        fields = ('old_password', 'new_password1', 'new_password2')
