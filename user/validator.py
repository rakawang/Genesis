from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model

User = get_user_model()

# function validating email i.e. checks if emails already exist.
def validate_email(value):
    if User.objects.filter(email = value).exists():
        raise ValidationError(
            (f"{value} already exists. Please use another email address."),
            params = {'value':value}
        )

def validate_username(value):
    if User.objects.filter(username = value).exists():
        raise ValidationError(
            (f"{value} already exists. Please choose another username."),
            params = {'value':value}
        )