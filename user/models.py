from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from PIL import Image
from django.utils.safestring import mark_safe

class MyUserManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            username = username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password=None):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            username = username,
            password=password,
        )

        user.is_admin = True
        user.is_client = True
        user.save(using=self._db)
        return user


class customUser(AbstractBaseUser):
    username = models.CharField(
        verbose_name='username',
        max_length=255,
        unique=True,
        blank=False,
        null=False,
        default = "New User",
    )
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    is_client = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin



# Profile

class Profile(models.Model):
    user = models.OneToOneField(customUser, on_delete=models.CASCADE)
    fullname = models.CharField(max_length = 255, blank=True)
    phone = models.CharField(blank=True, max_length=20)
    address = models.CharField(blank=True, max_length=150)
    city = models.CharField(blank=True, max_length=20)
    country = models.CharField(blank=True, max_length=50)
    image = models.ImageField(blank=True, null=True,upload_to='users/profileImage', default = 'profileImage/default-profile-image.png')
    
    def __str__(self):
        return  f'{self.user} profile'

    @property
    def imageURL(self):
        if self.image:
            try:
                url = self.image.url
            except:
                url = ''
        return url

# Function that reshapes image    
    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        if self.image:
            img = Image.open(self.image.path)

            if img.height > 300 or  img.width > 300:
                new_img = (300, 300)
                img.thumbnail(new_img)
                img.save(self.image.path)

    @property
    def image_tag(self):
        if self.image:
            return mark_safe('<img src="{}" width="35" height="40" />'.format(self.image.url))
        return ""
