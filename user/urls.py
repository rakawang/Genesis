from django.urls import path
from .views import (Registration,
                    Login, 
                    Logout, 
                    index, 
                    ProfileView,
                    UpdateProfileView,
                    DeleteUserView,
                    PasswordChangeView,
                    PasswordResetView,
                    PasswordResetDoneView,
                    PasswordResetConfirmView,
                    PasswordResetCompleteView,
                    ClientRegistration)

from . import views


app_name = 'user'

urlpatterns = [
    path('clientRegister/', ClientRegistration.as_view(), name='client_registration'),
    path('register/', Registration.as_view(), name='register'),
    path('login/', Login.as_view(), name='login'),
    path('logout/', Logout.as_view(), name='logout'),
    path('index/', index.as_view(), name='index'),
    path('profile/<int:pk>', ProfileView.as_view(), name='profile'),
    path('update_profile/<int:pk>', UpdateProfileView.as_view(), name='update_profile'),
    path('delete_user/<int:pk>', DeleteUserView.as_view(), name='delete_user'),
    path('change_password/', PasswordChangeView.as_view(), name='change_password'),
    path('password_reset/', PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('password_reset_confirm/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('password_reset_complete/', PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]
