from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserChangeForm, UserCreationForm
from .models import customUser, Profile

class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'is_client', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        ('Client', {'fields': ('is_client',)}),
        ('Permissions', {'fields': ('is_admin','is_active')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2'),
        }),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


class profileAdmin(admin.ModelAdmin):
    list_display = ('profile_username','id', 'fullname', 'address', 'country', 'image_tag' )
    readonly_fields = ('image_tag',)

    def image_tag(self, obj):
        return obj.image_tag

    image_tag.short_description = 'Profile Pic'
    image_tag.allow_tags = True

    def profile_username(self, obj):
        return obj.user
    
    profile_username.short_description = 'Username'

# Now register the new UserAdmin...
admin.site.register(customUser, UserAdmin)
admin.site.register(Profile, profileAdmin)

# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)