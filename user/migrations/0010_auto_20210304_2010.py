# Generated by Django 3.1.7 on 2021-03-04 14:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0009_auto_20210304_1923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='image',
            field=models.ImageField(blank=True, default='profileImage/default-profile-image.png', upload_to='users/profileImage'),
        ),
    ]
