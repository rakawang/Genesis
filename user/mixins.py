from django.utils.http import is_safe_url


# mixins for next redirect 
class NextUrlMixin(object):

    def get_next_url(self):
        next_ = self.request.GET.get('redirect_to')
        next_post = self.request.POST.get('redirect_to')
        redirect_path = next_ or next_post or None

        if is_safe_url(redirect_path, self.request.get_host()):
            return redirect_path
        return self.get_success_url()