from django.shortcuts import redirect, render
from django.views.generic import (View, 
								  CreateView, 
								  UpdateView,
								  DetailView,
								  DeleteView,
								  TemplateView,
								  FormView)

from .forms import (UserCreationForm, 
					UserLoginForm, 
					normalUserCreation, 
					ProfileUpdateForm,
					passwordChangeForm)

from django.contrib.auth import (
							authenticate,
							login,
							logout)

from django.contrib.auth.views import (
								PasswordChangeView, 
								PasswordResetView,
								PasswordResetDoneView,
								PasswordResetConfirmView,
								PasswordResetCompleteView)
									
from django.urls import reverse_lazy
from django.http.response import HttpResponseRedirect
from Genesis import settings
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from django.utils.http import is_safe_url, urlsafe_base64_encode
from django.contrib.auth.views import redirect_to_login
from django.http import Http404

# for token generation
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm

from .models import customUser, Profile
from .mixins import NextUrlMixin

# to access system varibale
import os
from django.core.mail import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes

# permission denied custom template
def error_403(request, exception):
        data = {'forbiddenUser': request.user}
        return render(request,'user/403.html', data)

# page not found
def error_404(request, exception):
        data = {}
        return render(request,'user/404.html', data)

class Registration(SuccessMessageMixin, CreateView):
	form_class = normalUserCreation
	template_name = 'user/Registration.html'
	success_url = reverse_lazy('user:login')
	success_message = "Account for %(username)s was created successfully"

	def form_valid(self, form):
		user = form.save(commit=False)
		user.save()
		mail_subject = 'Registration Successful.'
		message = render_to_string('user/welcome_email.html', {
			'user': user,
		})
		to_email = form.cleaned_data.get('email')
		email = EmailMessage(
			mail_subject, message, to=[to_email]
		)
		email.send()
		return super(Registration, self).form_valid(form)

class ClientRegistration(SuccessMessageMixin, CreateView):
	form_class = UserCreationForm
	template_name = 'user/Registration.html'
	success_url = reverse_lazy('user:login')
	success_message = "Account for %(username)s was created successfully"


class Login(NextUrlMixin, FormView):
	form_class= UserLoginForm
	template_name = 'user/Login.html'
	success_url = reverse_lazy('user:index')
		
	# function to redirect to next url
	# def get_next_url(self):
	# 	next_ = self.request.GET.get('redirect_to')
	# 	next_post = self.request.POST.get('redirect_to')
	# 	redirect_path = next_ or next_post or None
	# 	if is_safe_url(redirect_path, self.request.get_host()):
	# 		return redirect_path

	# 	return self.get_success_url()
		
	def form_valid(self, form):
		username=form.cleaned_data.get('username')
		password = form.cleaned_data.get('password')
		user = authenticate(username=username, password=password)
		login(self.request, user)

		return HttpResponseRedirect(self.get_next_url())

	# for already logged user
	def get(self, request, *args, **kwargs):
		if request.user.is_authenticated:
			return redirect(settings.LOGIN_REDIRECT_URL)
		return super(Login, self).get(request, *args, **kwargs)

class Logout(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(settings.LOGIN_URL)

class index(TemplateView):
    template_name = 'base.html'

# ----------------------------PROFILE-------------------------------

class ProfileView(LoginRequiredMixin,DetailView):
	model = Profile
	template_name = 'user/Profile.html'
	login_url = 'user:login'
	redirect_field_name = 'redirect_to'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['userProfile'] = Profile.objects.get(pk = self.kwargs['pk'])
		return context

class UpdateProfileView(UserPassesTestMixin, LoginRequiredMixin, UpdateView):
	model = Profile
	form_class = ProfileUpdateForm
	template_name = 'user/UpdateProfile.html'
	pk_url_kwarg = 'pk'
	login_url = 'user:login'
	redirect_field_name = 'redirect_to'

	def test_func(self):
		obj = self.get_object()
		if obj.user == self.request.user:
			return True
		return False

	def get_object(self, queryset=None):
		profile = self.model.objects.get(pk= self.kwargs['pk'])
		return profile

	def get_success_url(self):
		return reverse_lazy('user:profile', kwargs={'pk': self.object.pk})

class DeleteUserView(UserPassesTestMixin, LoginRequiredMixin, DeleteView):
	model = customUser
	context_object_name = 'user'
	pk_url_kwarg = 'pk'
	template_name = 'user/confirm_delete_user.html'
	success_url = reverse_lazy('user:index')
	login_url = 'user:login'
	redirect_field_name = 'redirect_to'

	def test_func(self):
		obj = self.get_object()
		return obj == self.request.user
 
	def get_object(self, queryset=None):
		user = self.model.objects.get(pk= self.kwargs['pk'])
		return user

# ---------------------- Password change ----------------------
class PasswordChangeView(LoginRequiredMixin, PasswordChangeView):
	model = customUser
	form_class = passwordChangeForm
	template_name = 'user/passwordChange.html'
	success_url = reverse_lazy('user:login') 
	login_url = 'user:login'
	redirect_field_name = 'redirect_to'

class PasswordResetView(PasswordResetView):
	template_name = 'user/password_reset.html'
	email_template_name = 'user/password_reset_email.html'
	# subject_template_name = 'user/password_reset_subject.txt'
	form_class = PasswordResetForm
	from_email = settings.EMAIL_HOST_USER
	token_generator = default_token_generator
	html_email_template_name = None
	extra_email_context = None
	success_url = reverse_lazy('user:password_reset_done')

	# backend process of password rest view  

	# def form_valid(self, form):
	# 	opts = {
	# 		'use_https': self.request.is_secure(),
	# 		'token_generator': self.token_generator,
	# 		'from_email': self.from_email,
	# 		'email_template_name': self.email_template_name,
	# 		# 'subject_template_name': self.subject_template_name,
	# 		'request': self.request,
	# 		'html_email_template_name': self.html_email_template_name,
	# 		'extra_email_context': self.extra_email_context,
	# 	}
	# 	form.save(**opts)
	# 	return super().form_valid(form)
	
	# def get_success_url(self):
	#  	return reverse_lazy('user:password_reset_done')

class PasswordResetDoneView(PasswordResetDoneView):
	template_name = 'user/password_reset_done.html'

class PasswordResetConfirmView(PasswordResetConfirmView):
	template_name = 'user/password_reset_confirm.html'
	form_class = SetPasswordForm
	token_generator = default_token_generator
	reset_url_token = 'set-password'
	post_reset_login = False
	success_url = reverse_lazy('user:password_reset_complete')

class PasswordResetCompleteView(PasswordResetCompleteView):
	template_name = 'user/password_reset_complete.html'

	
